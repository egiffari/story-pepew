from django.urls import path
from . import views


app_name = 'homepage'

urlpatterns = [
    path('profile/', views.index2, name='index2'),
    path('skills/', views.index3, name='index3'),
    path('', views.index, name='index')   
]